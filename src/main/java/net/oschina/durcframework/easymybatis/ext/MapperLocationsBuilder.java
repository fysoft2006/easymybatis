/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.oschina.durcframework.easymybatis.ext;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.QName;
import org.dom4j.dom.DOMAttribute;
import org.dom4j.io.SAXReader;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.StringUtils;

import net.oschina.durcframework.easymybatis.EasymybatisConfig;
import net.oschina.durcframework.easymybatis.dao.Dao;
import net.oschina.durcframework.easymybatis.ext.code.client.ClassClient;

/**
 * mapper构建
 * @author tanghc
 *
 */
public class MapperLocationsBuilder {

	private static Log LOGGER = LogFactory.getLog(MapperLocationsBuilder.class);
	private static final String ENCODE = "UTF-8";
	private static final String XML_SUFFIX = ".xml";
	private static final String MAPPER_START = "<mapper>";
	private static final String MAPPER_END = "</mapper>";
	
	private static final String TEMPLATE_SUFFIX = ".vm";
	
	private static String DEFAULT_CLASS_PATH = "/easymybatis/tpl/";
	
	private Map<String, MapperResourceDefinition> mapperResourceStore = new HashMap<>();
	
	private EasymybatisConfig config = new EasymybatisConfig();
	
	private Attribute NAMESPACE;
	
	private String dbName;
	
	
	/**
	 * 初始化mybatis配置文件
	 * 
	 * @param basePackage
	 *            实体类的包目录.指定哪些包需要被扫描,支持多个包"package.a,package.b"并对每个包都会递归搜索
	 */
	public Resource[] build(String basePackage) {
		init();
		try {
			String[] basePackages = StringUtils.tokenizeToStringArray(basePackage,
					ConfigurableApplicationContext.CONFIG_LOCATION_DELIMITERS);
			ClassScanner classScanner = new ClassScanner(basePackages, Dao.class);
			Set<Class<?>> clazzsSet = classScanner.getClassSet();

			Resource[] mapperLocations = this.buildMapperLocations(clazzsSet);

			return mapperLocations;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}finally {
			distroy();
		}
	}
	
	private void init() {
		NAMESPACE = new DOMAttribute(new QName("namespace"));
	}
	
	private void distroy() {
		NAMESPACE = null;
		mapperResourceStore.clear();
	}

	
	// 存储xml文件
	public void storeMapperFile(Resource[] mapperLocations) {
		for (Resource mapperLocation : mapperLocations) {
			String filename = mapperLocation.getFilename();// XxDao.xml
			
			mapperResourceStore.put(filename, new MapperResourceDefinition(mapperLocation));
		}
	}
	
	private MapperResourceDefinition getMapperFile(String mapperFileName) {
		return mapperResourceStore.get(mapperFileName);
	}

	private Resource[] buildMapperLocations(Set<Class<?>> clazzsSet) {
		
		List<Resource> mapperLocations = this.buildMapperResource(clazzsSet);
		
		this.addUnmergedResource(mapperLocations);
		
		this.addCommonSqlClasspathMapper(mapperLocations);

		return mapperLocations.toArray(new Resource[mapperLocations.size()]);
	}
	
	private List<Resource> buildMapperResource(Set<Class<?>> clazzsSet) {
		final String templateClasspath = this.buildTemplateClasspath(this.getDbName());
		final ClassClient codeClient = new ClassClient(config);
		final List<Resource> mapperLocations = Collections.synchronizedList(new ArrayList<Resource>(clazzsSet.size()));
		int poolSize = config.getMapperExecutorPoolSize();
		ExecutorService executorPool = Executors.newFixedThreadPool(poolSize > clazzsSet.size() ? clazzsSet.size() : poolSize);
		long startTime = System.currentTimeMillis();
		
		for (Class<?> mapperClass : clazzsSet) {
			final Class<?> daoClass = mapperClass;
			executorPool.execute(new Runnable() {
				@Override
				public void run() {
					String xml = codeClient.genMybatisXml(daoClass, templateClasspath);
					
					xml = mergeExtMapperFile(daoClass, xml);
					
					Resource resource;
					try {
						resource = new MapperResource(xml, daoClass);
						mapperLocations.add(resource);
					} catch (UnsupportedEncodingException e) {
						LOGGER.error(e.getMessage(),e);
						throw new RuntimeException(e);
					}
				}
			});
		}
		
		try {
			// 等待线程池中所有的线程都执行完毕
			executorPool.shutdown();
			executorPool.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
			
			long endTime = System.currentTimeMillis();
			LOGGER.info("生成Mapper内容总耗时：" + (endTime - startTime)/1000.0 + "秒");
			
			this.saveMapper(config.getMapperSaveDir(), mapperLocations);
			
			return mapperLocations;
		} catch (InterruptedException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
		
	}
	
	// 保存mapper到本地文件夹
	private void saveMapper(final String saveDir,final List<Resource> mapperLocations) {
		if(StringUtils.hasText(saveDir)) {
			try {
				LOGGER.info("保存mapper文件到" + saveDir);
				for (Resource resource : mapperLocations) {
					OutputStream out = new FileOutputStream(saveDir + "/" +  resource.getFilename());
					IOUtils.copy(resource.getInputStream(), out);
					out.close();
				}
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
		}
	}
	
	private String buildTemplateClasspath(String dbName) {
		String templateClasspath = config.getTemplateClasspath();
		if(templateClasspath != null) {
			return templateClasspath;
		}
		// 返回格式：classpath路径 + 数据库名称 + 文件后缀
		// 如：/easymybatis/tpl/mysql.vm
		return DEFAULT_CLASS_PATH + this.buildTemplateFileName(dbName);
	}
	
	// 构建文件名
	private String buildTemplateFileName(String dbName) {
		dbName = dbName.replaceAll("\\s", "").toLowerCase();
		return dbName + TEMPLATE_SUFFIX;
	}
	
	private void addCommonSqlClasspathMapper(List<Resource> mapperLocations) {
		String commonSqlClasspath = config.getCommonSqlClasspath();
		ClassPathResource res = new ClassPathResource(commonSqlClasspath);
		mapperLocations.add(res);
	}

	// 合并其它mapper
	private void addUnmergedResource(List<Resource> mapperLocations) {
		Collection<MapperResourceDefinition> mapperResourceDefinitions = this.mapperResourceStore.values();
		for (MapperResourceDefinition mapperResourceDefinition : mapperResourceDefinitions) {
			if(mapperResourceDefinition.isMerged()) {
				continue;
			}
			
			LOGGER.info("加载未合并Mapper：" + mapperResourceDefinition.getFilename());
			
			mapperLocations.add(mapperResourceDefinition.getResource());
		}
	}
	
	// 合并扩展mapper文件内容
	private String mergeExtMapperFile(Class<?> mapperClass, String xml) {
		// 自定义文件
		String mapperFileName = mapperClass.getSimpleName() + XML_SUFFIX;
		MapperResourceDefinition mapperResourceDefinition = this.getMapperFile(mapperFileName);

		if (mapperResourceDefinition == null) {
			return xml;
		}
		StringBuilder sb = new StringBuilder();

		sb.append(xml.replace(MAPPER_END, "")); // 先去掉</mapper>
		// 追加内容
		String extFileContent = this.getExtFileContent(mapperResourceDefinition.getResource());
		
		sb.append(extFileContent).append(MAPPER_END); // 追加自定义sql,加上</mapper>
		
		mapperResourceDefinition.setMerged(true);

		return sb.toString();

	}

	private String getExtFileContent(Resource mapperLocation) {
		try {
			InputStream in = mapperLocation.getInputStream();
		    Document document = this.buildSAXReader().read(in); 
		    Element mapperNode = document.getRootElement();
		    
		    String rootNodeName = mapperNode.getName();
		    
		    if(!"mapper".equals(rootNodeName)) {
		    	throw new Exception("mapper文件必须含有<mapper>节点,是否缺少<mapper></mapper>?");
		    }
		    
		    mapperNode.remove(NAMESPACE);
		    
		    String rootXml = mapperNode.asXML();
		    // 去除首尾mapper标签
		    rootXml = rootXml.replace(MAPPER_START, "").replace(MAPPER_END, "");
		    
			return rootXml;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			throw new RuntimeException("加载资源文件出错,[" + mapperLocation.getFilename() + "]," + e.getMessage());
		} 
	}
	
	private SAXReader buildSAXReader() {
		SAXReader reader = new SAXReader();
		reader.setEncoding(ENCODE);
		return reader;
	}
	
	public void setConfig(EasymybatisConfig config) {
		this.config = config;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public void setMapperExecutorPoolSize(int poolSize) {
		config.setMapperExecutorPoolSize(poolSize);
	}

	// MapperResource包装类
	private static class MapperResourceDefinition {
		// 是否合并
		private boolean merged;
		// mapper对应的resource
		private Resource resource;

		public MapperResourceDefinition(Resource resource) {
			super();
			this.resource = resource;
		}
		
		public String getFilename() {
			return this.resource.getFilename();
		}

		public boolean isMerged() {
			return merged;
		}

		public void setMerged(boolean merged) {
			this.merged = merged;
		}

		public Resource getResource() {
			return resource;
		}
	}
	
	// 不能用InputStreamResource，因为InputStreamResource只能read一次
	private static class MapperResource extends ByteArrayResource {
		private Class<?> mapperClass;

		public MapperResource(String xml,Class<?> mapperClass) throws UnsupportedEncodingException {
			 /*	必须指明InputStreamResource的description
			  	主要解决多个dao报Invalid bound statement (not found)BUG。
			  	经排查是由XMLMapperBuilder.parse()中，configuration.isResourceLoaded(resource)返回true导致。
			  	因为InputStreamResource.toString()始终返回同一个值，需要指定description保证InputStreamResource.toString()唯一。
			  */
			super(xml.getBytes(ENCODE), mapperClass.getName());
			this.mapperClass = mapperClass;
		}
		
		@Override
		public String getFilename() {
			return this.mapperClass.getSimpleName() + XML_SUFFIX;
		}
		
	}


}
